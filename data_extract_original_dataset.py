"""
*Intent:
    This program is used to extract citing sentences-only from original dataset.
    Original dataset file name: all-txt-for-elasticsearch.txt
    Source paper: https://www.aclweb.org/anthology/L18-1296.pdf
"""

import glob
import os
import re

#path = os.getcwd()
path = "../experiment/original_data/"
files = [f for f in glob.glob(path + "**/*.txt", recursive=True)]
listTotal = list()

num_files = 0
num_sentence = 0
num_all_sentences = 0

for f in files:
    file_txt = open(f, 'r')
    num_files += 1

    for baris in file_txt:
        if ("<DBLP:" or "<GC:") in baris:
            baris = baris.split("\t")
            try:
                str = "".join(baris[4])
                start = re.escape("<DBLP:")
                end = re.escape(">")
                result = re.search('%s(.*)%s' % (start, end), str).group(1)
                result = str.replace(result, "citation")
                result = result.replace("DBLP:", "")

                str = result
                start = re.escape("<GC:")
                end = re.escape(">")
                result = re.search('%s(.*)%s' % (start, end), str).group(1)
                result = str.replace(result, "citation")
                result = result.replace("GC:", "")

                print(result)
                listTotal.append(result)
            except:
                listTotal.append("null")
            num_sentence += 1

        num_all_sentences += 1


with open("../experiment/working_data/extracted_citing_sentences.txt", "w") as output_file:
    for line in listTotal:
        if line != "null":
            output_file.write(line)

print('Jumlah File:  ', num_files)
print('Number of all Sentences:   ', num_all_sentences)
print('Number of Citing Sentences:   ', num_sentence)

