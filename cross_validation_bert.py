"""
This program is used for performing BERT-based classification
the classification performance is conducted by 5 fold cross validation
source:
(1) library: https://github.com/amaiya/ktrain
(2) paper: https://arxiv.org/pdf/2004.10703.pdf
This program is inspired by:
https://github.com/amaiya/ktrain/blob/master/tutorials/tutorial-A3-hugging_face_transformers.ipynb

status: ready for experiment
"""
import time
import numpy as np
from sklearn.model_selection import KFold

import ktrain
from ktrain import text

from UtilTextPreprocess import load_data
from UtilTextPreprocess import text_preprocessing
from sklearn.model_selection import train_test_split

# get start time
start_time = time.time()
print("process starting at: ", start_time)

# load experiment training data
#path = "Experiment/Data/SeedExperimentNew.csv"
path = "Experiment/Data/Augmented/AugmentedSize8Alpha04.csv"
X, y = load_data(path)

# pre-process the train data (X only)
X = text_preprocessing(X)

categories = [
    "definition",
    "suggest",
    "judgement",
    "technical",
    "trend",
    "citing_paper_propose",
    "based_on",
    "use",
    "extend",
    "citing_paper_success",
    "cited_paper_propose",
    "cited_paper_success",
    "cited_paper_weakness",
    "cited_paper_result",
    "compare",
    "contrast",
    "other"
]

# split for corss validation
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, stratify=y, random_state=42)

# we use 80% of data to perform cross validation
X = np.array(X_train)
y = np.array(y_train)

# 5 fold
kf = KFold(n_splits=5, random_state=None, shuffle=False)

# perform 5 fold cross validation
for train_index, val_index in kf.split(X):
    X_train, X_test = X[train_index], X[val_index]
    y_train, y_test = y[train_index], y[val_index]

    # other option https://huggingface.co/transformers/pretrained_models.html
    MODEL_NAME = 'bert-base-uncased'
    t = text.Transformer(MODEL_NAME, maxlen=100, class_names=categories)
    trn = t.preprocess_train(X_train, y_train)
    val = t.preprocess_test(X_test, y_test)
    model = t.get_classifier()
    learner = ktrain.get_learner(model, train_data=trn, val_data=val, batch_size=32)
    # estimate good learning rate
    #learner.lr_find(show_plot=True, max_epochs=2)
    # learning rate and epoch
    learner.fit_onecycle(2e-5, 8)
    # validation
    learner.validate(class_names=t.get_classes())

# get end time
print("--- %s seconds ---" % (time.time() - start_time))