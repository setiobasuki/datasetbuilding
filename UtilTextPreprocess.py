"""
This program is used for:
1. convert label to integer and vice versa
2. text proprocessing
"""

# to convert labels to integers
label2int = {"definition" : 0,
"suggest" : 1,
"judgement" : 2,
"technical" : 3,
"trend" : 4,
"citing_paper_propose" : 5,
"based_on" : 6,
"use" : 7,
"extend" : 8,
"citing_paper_success" : 9,
"cited_paper_propose" : 10,
"cited_paper_success" : 11,
"cited_paper_weakness" : 12,
"cited_paper_result" : 13,
"compare" : 14,
"contrast" : 15,
"other": 16}




# to convert integer to label
int2label = {0: "definition",
1: "suggest",
2: "judgement",
3: "technical",
4: "trend",
5: "citing_paper_propose",
6: "based_on",
7: "use",
8: "extend",
9: "citing_paper_success",
10: "cited_paper_propose",
11: "cited_paper_success",
12: "cited_paper_weakness",
13: "cited_paper_result",
14: "compare",
15: "contrast",
16: "other"}

import csv

# load the CSV file
def load_data(path):
    """
    Load Citing Sentences data, CSV Format
    """
    texts, labels = [], []
    with open(path, 'r', encoding='utf-8-sig') as f:
        csvData = csv.reader(f)
        for lines in csvData:
            labels.append(lines[0])
            texts.append(lines[1])
    return texts, labels

import string
import numpy as np
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer

# lowercase, punctuation removal, stemming, and lemmatization
def text_preprocessing(text):
    """
    these following 2 lines of code is executed once
    """
    #nltk.download('punkt')
    #nltk.download('wordnet')
    #nltk.download('stopwords')

    # if we want to use stopword removal.
    #stop_words = stopwords.words('english') + list(string.punctuation.replace(",","")) + list(['’', '”'])
    #stop_words = stopwords.words('english') + list(string.punctuation) + list(['’', '”'])

    # if we don't use stopword removal (this scenario give better result)
    #stop_words = set(string.punctuation)-{',','<', '>'}
    stop_words = list(string.punctuation.replace(",","").replace("<","").replace(">","")) + list(['’', '”', '"\"'])
    ps = PorterStemmer()
    lemmatizer = WordNetLemmatizer()
    # stopwords removal
    new_X = []
    for sentence in text:
        word_tokens = word_tokenize(sentence.lower())
        # filtered_sentence = [w for w in word_tokens if not w in stop_words]
        filtered_sentence = [ps.stem(w) for w in word_tokens if not w in stop_words]

        filtered_sentence = []
        for w in word_tokens:
            if w not in stop_words:
                # filtered_sentence.append(w)
                filtered_sentence.append(lemmatizer.lemmatize(w))
        new_X.append(" ".join(filtered_sentence))
    final_preprocessed = np.array(new_X)
    return final_preprocessed

# this method only for experiment to preprocess dataset
# load experiment training data
path = "experiment/working_data/SeedExperimentNew.csv"
X, y = load_data(path)

# pre-process the train data (X only)
X = text_preprocessing(X)

# print preprocessed X
for i in X:
    print(i)