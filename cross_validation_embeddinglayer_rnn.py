"""
* Intent:
    This program is used for cross validation on seed training dataset by using
    embedding layer on RNN Keras.
"""

from UtilTextPreprocess import load_data
from UtilTextPreprocess import text_preprocessing
from UtilTextPreprocess import label2int

import nltk
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, SpatialDropout1D
from keras.layers import SimpleRNN
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from keras.wrappers.scikit_learn import KerasClassifier

# embedding hyper-parameter
OUTPUT_LAYER = 17
MAX_INPUT_LENGTH = 100
N_DIM = 64
DROP_EMBED = 0.2
# rnn hyper-parameter
N_RNN = 256
DROP_RNN = 0.2
# dense layer hyper-parameter
N_DENSE = 10
# training hyper-parameter
BATCH = 32
EPOCHS = 8

def building_keras_model():
    # building the model
    model = Sequential()
    model.add(Embedding(vocab_size, N_DIM, input_length=MAX_INPUT_LENGTH))
    model.add(SpatialDropout1D(DROP_EMBED))
    model.add(SimpleRNN(N_RNN, dropout=DROP_RNN))
    model.add(Dense(N_DENSE, activation='relu'))
    model.add(Dense(OUTPUT_LAYER, activation='sigmoid'))
    model.summary()
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    model.fit(X, y, batch_size=BATCH, epochs=EPOCHS, verbose=1)
    return model

# load experiment training data
path = "experiment/working_data/AugmentedSize8Alpha05.csv"
#path = "Experiment/Data/Augmented/AugmentedSize8Alpha03.csv"
X, y = load_data(path)

# pre-process the train data (X only)
X = text_preprocessing(X)

# Text Tokenization
tokenizer = Tokenizer()
tokenizer.fit_on_texts(X)
vocab_size = len(tokenizer.word_index) + 1

# Vectoring text, turning the text into sequence of integers
X = tokenizer.texts_to_sequences(X)

# Tokenized data doesn't have the same length, we need a fixed length sequence
X = np.array(X)
X = pad_sequences(X, maxlen=100)

# Label encoding
y = np.array(y)
y = [label2int[label] for label in y]
y = to_categorical(y)

# building the keras classifier
classifier = KerasClassifier(building_keras_model)

# This is for Cross Validation
print("Cross Validation: ", cross_val_score(classifier, X, y, cv=5))
