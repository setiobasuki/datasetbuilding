"""
This program is used for performing ULMFiT-based classification
using 5 fold cross validation
This program is inspired by:
(1) https://github.com/aditya10/ULMFiT-fastai-text-classifier/blob/master/ULMFiT_tutorial.ipynb
(2) https://github.com/prateekjoshi565/ULMFiT_Text_Classification/blob/master/ULMFiT_fastai_Text_Classification.ipynb
cross validation is inspired from https://medium.com/swlh/k-fold-as-cross-validation-with-a-bert-text-classification-example-4017f76a863a
status: ready for experiment
"""

from fastai.text import *
from UtilTextPreprocess import load_data
from UtilTextPreprocess import text_preprocessing
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold

# load experiment training data
#path = "experiment/working_data/AugmentedSize8Alpha01.csv"
path = "Experiment/Data/Augmented/AugmentedSize8Alpha05.csv"
X, y = load_data(path)

# pre-process the train data (X only)
X = text_preprocessing(X)

# split for corss validation
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, stratify=y, random_state=42)

# convert numpy array to pandas dataframe
X = pd.DataFrame(X_train)
y = pd.DataFrame(y_train)

# join the X and y
df = pd.concat([y, X], axis=1, sort=False)
# print('Length of dataset: '+str(len(df.index)))
print("Length of dataset: ", len(df.index))

# prepare 5 cross validation
kf = KFold(n_splits=5, random_state=None, shuffle=True)

# looping for cross validation
for train_index, val_index in kf.split(df):
    train_df = df.iloc[train_index]
    val_df = df.iloc[val_index]

    # split train and validation
    #df_trn, df_val = train_test_split(df, stratify=y, test_size=0.2)
    print(f'{train_df.shape}{val_df.shape}')

    # Language model data
    data_lm = TextLMDataBunch.from_df(train_df=train_df, valid_df=val_df, path="")

    # Classifier model data
    data_clas = TextClasDataBunch.from_df(
        train_df=train_df,
        valid_df=val_df,
        vocab=data_lm.train_ds.vocab,
        bs=32,
        path=""
    )

    # create and train the language model
    learn = language_model_learner(data_lm, AWD_LSTM, drop_mult=0.3)
    learn.fit_one_cycle(8, 1e-2)
