"""
This program is used to implement cross validation with linear regression using feature:
1. bow
2. tf-idf
3. pre-trained: fasttext, glove, and word2vec
STATUS: ready for experiment
"""

import warnings
warnings.filterwarnings("ignore")
warnings.filterwarnings(action="ignore",category=DeprecationWarning)
warnings.filterwarnings(action="ignore",category=FutureWarning)

from UtilTextPreprocess import load_data
from UtilTextPreprocess import text_preprocessing
from UtilTextPreprocess import label2int

import numpy as np
from numpy import asarray
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from keras.preprocessing.text import Tokenizer
from numpy import zeros

# load experiment training data
path = "Experiment/Data/SeedExperimentNew.csv" # original dataset
#path = "Experiment/Data/Augmented/AugmentedSize8Alpha01.csv"
X, y = load_data(path)

# Label encoding
y = np.array(y)
y = [label2int[label] for label in y]

# pre-process the train data (X only)
X = text_preprocessing(X)

# Text Tokenization
tokenizer = Tokenizer()
tokenizer.fit_on_texts(X)
vocab_size = len(tokenizer.word_index) + 1

# train and test splitting
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, stratify=y, random_state=123)

# --------------------------------------
# BAG OF WORD (BOW) FEATURES
# --------------------------------------

# BOW without pipeline
vectC = CountVectorizer(ngram_range=(1,2))
vectC.fit(X_train)
bag_of_word = vectC.transform(X_train)
print("BOW cross validation without Pipeline:")
print(cross_val_score(LogisticRegression(penalty="l1", solver='liblinear'), bag_of_word, y_train, cv=5).mean())

# BOW with pipeline
bow_pipeline = Pipeline([
    ('vectorize', CountVectorizer()),
    ('clf', LogisticRegression(penalty="l1", solver='liblinear')),
])

bow_parameters = {
    'vectorize__ngram_range': ((1, 1), (1, 2), (1,3)),
    'clf__C': (0.1, 0.01, 0.1, 1, 10),
    'clf__penalty': ['l1'],
}

bow_grid_search = GridSearchCV(estimator=bow_pipeline, param_grid=bow_parameters, cv=5)
bow_grid_search.fit(X_train, y_train)
print("BOW cross validation with Pipeline:")
print(bow_grid_search.best_score_)
print(bow_grid_search.best_params_)

# --------------------------------------
# TF-IDF FEATURES
# --------------------------------------
pipe = make_pipeline(TfidfVectorizer(), LogisticRegression(penalty='l1', solver='liblinear'))
param_grid = {"logisticregression__C": (0.1, 0.01, 0.1, 1, 10), "tfidfvectorizer__ngram_range": ((1, 1),(1, 2),(1,3))}
grid = GridSearchCV(pipe, param_grid, cv=5)
grid.fit(X_train, y_train)
print("TFIDF cross validation with Pipeline:")
print("Best score: ", grid.best_score_)
print("Best parameter: ", grid.best_params_)

# ---------------------------------------------
# feature --> pre-trained features
# ---------------------------------------------

# convert numpy array to pandas dataframe
import pandas as pd
from nltk import word_tokenize

columns = [f'col_{num}' for num in range(1)]
index = [f'index_{num}' for num in range(X_train.shape[0])]
df = pd.DataFrame(X_train, columns=columns, index=index)
X_train = df['col_0'].map(word_tokenize).values
total_vocabulary = set(word for headline in X_train for word in headline)

embeddings_index = dict()
#path = "Experiment/PreTrained/glove.840B.300d.txt" # glove
#path = "Experiment/PreTrained/crawl-300d-2M.vec" # fasttext, faster than wiki-news version
path = "Experiment/PreTrained/GoogleNews-vectors-negative300.bin.gz" # google pretrained word2vec

"""
# this is used for glove and fasttext, when only vector in total_vocabulary will be used
f = open(path, encoding="utf-8")
for line in f:
    values = line.split(" ")
    word = values[0]
    if word in total_vocabulary: # remove this line if it is needed to load all vectors
        coefs = asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
f.close()
print('Loaded fasttext %s word vectors.' % len(embeddings_index))
"""

pre_trained = "word2vec" # It can be change, since it only signal control
if pre_trained == "word2vec": # for word2vec
    print("loading pretrained:", pre_trained)
    from gensim.models.keyedvectors import KeyedVectors
    word2vec_model = KeyedVectors.load_word2vec_format(path, binary=True)
    def getVector(str):
        if str in word2vec_model:
            embeddings_index[str] = word2vec_model[str]
            return word2vec_model[str]
        else:
            return None;
    def isInModel(str):
        return str in word2vec_model

    embedding_matrix = np.zeros((vocab_size, 300))
    for word, i in tokenizer.word_index.items():
        embedding_vector = getVector(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector
else: # for fasttext and glove
    print("loading pretrained:", pre_trained)
    f = open(path)
    f = open(path, encoding="utf-8")
    for line in f:
        values = line.split(" ")
        word = values[0]
        coefs = asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()

    print('Loaded %s word vectors.' % len(embeddings_index))

    # create a weight matrix for words in training docs
    embedding_matrix = zeros((vocab_size, 300))
    for word, i in tokenizer.word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

class PretrainedVectorizer(object):

    def __init__(self, w2v):
        # Takes in a dictionary of words and vectors as input
        self.w2v = w2v
        if len(w2v) == 0:
            self.dimensions = 0
        else:
            self.dimensions = len(w2v[next(iter(embeddings_index))])

    # Note: Even though it doesn't do anything, it's required that this object implement a fit method or else
    # it can't be used in a scikit-learn pipeline
    def fit(self, X, y):
        return self

    def transform(self, X):
        return np.array([
            np.mean([self.w2v[w] for w in words if w in self.w2v]
                   or [np.zeros(self.dimensions)], axis=0) for words in X])

# classic machine learning algorithms with pre-trained
pre_trained_lr = Pipeline([('pre-trained embedding', PretrainedVectorizer(embeddings_index)), ('logistic regression', LogisticRegression())])

models = [ ('logistic regression', pre_trained_lr)]
scores = [(name, cross_val_score(model, X_train, y_train, cv=5).mean()) for name, model, in models]
print("Pre-Trained cross validation:")
print("CV score: ", scores)
