"""
* Intent:
    This program is used for cross validation on seed training dataset using pre-trained with RNN
    The pre-trained vectors used in this experiment are:
        1. Fasttext
        2. GloVe
        3. Word2Vec
"""

from UtilTextPreprocess import load_data
from UtilTextPreprocess import text_preprocessing
from UtilTextPreprocess import label2int

import numpy as np
from numpy import asarray
from numpy import zeros
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, SpatialDropout1D
from keras.layers import SimpleRNN
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

from sklearn.model_selection import cross_val_score
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import train_test_split

# load experiment training data
#path = "Experiment/Data/SeedExperimentNew.csv"
#path = "Experiment/Data/SeedExperimentNewAugmented.csv"
path = "Experiment/Data/Augmented/AugmentedSize8Alpha01.csv"
X, y = load_data(path)

# pre-process the train data (X only)
X = text_preprocessing(X)

# Text Tokenization
tokenizer = Tokenizer()
tokenizer.fit_on_texts(X)
vocab_size = len(tokenizer.word_index) + 1

# Vectoring text, turning the text into sequence of integers
X = tokenizer.texts_to_sequences(X)

# Tokenized data doesn't have the same length, we need a fixed length sequence
X = np.array(X)
X = pad_sequences(X, maxlen=100)

# Label encoding
y = np.array(y)
y = [label2int[label] for label in y]
y = to_categorical(y)

# split for corss validation
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, stratify=y, random_state=42)

# get the vocabulary from X_train
total_vocabulary = set(word for headline in X_train for word in headline)

# load the whole fasttext or glove embedding into memory
embeddings_index = dict()
#path = "Experiment/PreTrained/glove.840B.300d.txt" # glove
#path = "Experiment/PreTrained/crawl-300d-2M.vec" # fasttext, faster than wiki-news version
path = "Experiment/PreTrained/GoogleNews-vectors-negative300.bin.gz" # google pretrained word2vec

pre_trained = "word2vec" # It can be change, since it only signal control
if pre_trained == "word2vec": # for word2vec
    print("loading pretrained:", pre_trained)
    from gensim.models.keyedvectors import KeyedVectors
    word2vec_model = KeyedVectors.load_word2vec_format(path, binary=True)
    def getVector(str):
        if str in word2vec_model:
            return word2vec_model[str]
        else:
            return None;
    def isInModel(str):
        return str in word2vec_model

    embedding_matrix = np.zeros((vocab_size, 300))
    for word, i in tokenizer.word_index.items():
        embedding_vector = getVector(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector
else: # for fasttext and glove
    print("loading pretrained:", pre_trained)
    f = open(path)
    f = open(path, encoding="utf-8")
    for line in f:
        values = line.split(" ")
        word = values[0]
        coefs = asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()

    print('Loaded %s word vectors.' % len(embeddings_index))

    # create a weight matrix for words in training docs
    embedding_matrix = zeros((vocab_size, 300))
    for word, i in tokenizer.word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

# embedding hyper-parameter
OUTPUT_LAYER = 17
MAX_INPUT_LENGTH = 100
N_DIM = 300
DROP_EMBED = 0.2
# rnn hyper-parameter
N_RNN = 256
DROP_RNN = 0.2
# dense layer hyper-parameter
N_DENSE = 10
# training hyper-parameter
BATCH = 32
EPOCHS = 8

print("embedding matrix shape: ", embedding_matrix.shape)
print()

# building rnn model with embedding layer
def building_embedding_keras_model():
    model = Sequential()
    model.add(Embedding(vocab_size, N_DIM, weights=[embedding_matrix], input_length=MAX_INPUT_LENGTH, trainable=False))
    model.add(SpatialDropout1D(DROP_EMBED))
    model.add(SimpleRNN(N_RNN, dropout=DROP_RNN))
    model.add(Dense(N_DENSE, activation='relu'))
    model.add(Dense(OUTPUT_LAYER, activation='sigmoid'))
    model.summary()
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    model.fit(X, y, batch_size=BATCH, epochs=EPOCHS, verbose=1)
    return model

# building the keras classifier
classifier = KerasClassifier(building_embedding_keras_model)

# This is for Cross Validation
print("Cross Validation of ", pre_trained, " ", cross_val_score(classifier, X_train, y_train, cv=5).mean())
